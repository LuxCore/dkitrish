/**
 * This package contains concrete implementations of figure.
 *
 * @author Denis.Kitrish (Denis.Kitrish@Yandex.ua)
 * @since 21.01.2018
 * @version 1.0
 */
package ru.job4j.chess.figure;
